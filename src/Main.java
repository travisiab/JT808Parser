import cn.sih.parse.*;

import java.io.*;

public class Main {


    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out = new PrintWriter(System.out);
        out.println("\n########808 message parser############");
        out.println("Please enter hex source string, or input 'exit' to quite.");
        out.flush();

        String line = null;
        while ((line = br.readLine()) != null) {
            if ("exit".equals(line))
                break;
            if ("file".equals(line)) {
                new Parser().doParse();
                continue;
            }
            if (line == null || "".equals(line.trim()))
                continue;
            String id = getMessageId(line);
            Message message = null;
            if ("0200".equals(id)) {
                message = new Message0x0200(line);
            } else if ("2036".equals(id)) {
                message = new Message0x2036(line);
            } else if ("2035".equals(id)) {
                message = new Message0x2035(line);

            } else if ("A031".equals(id)) {
                message = new Message0xA031(line);
            } else {
                out.println("not support message");
                continue;
            }
            message.parse();
            out.println(message.toString());
            out.println();
            out.flush();


        }
        out.close();
        br.close();
    }



    public static String getMessageId(String str) {
        return str.substring(2, 6);
    }
}
