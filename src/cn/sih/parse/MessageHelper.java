package cn.sih.parse;

/**
 * Created by travis on 15/10/16.
 */
public class MessageHelper {

    private String buffer;
    private int pos = 0;

    public MessageHelper(String buffer) {
        this.buffer = buffer;
    }

    public String peek() {
        return lookChar();
    }

    /**
     * 弹出以十六进制返回长度为2的字符串，例如7E。
     * @return
     */
    public String pop() {
        String ch = lookChar();
        pos += 2;
        return ch;
    }

    public String pop(int num) {
        String result = "";
        while (num-- > 0)
            result += pop();
        return result;
    }

    public void shift(int offset) {
        while (offset-- > 0)
            pos += 2;
    }


    private String lookChar() {
        if (buffer == null || pos >= buffer.length())
            return null;
        return buffer.substring(pos, pos + 2);
    }




    public String getBuffer() {
        return buffer;
    }

    public void setBuffer(String buffer) {
        this.buffer = buffer;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
