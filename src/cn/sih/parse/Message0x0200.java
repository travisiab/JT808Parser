package cn.sih.parse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by travis on 15/10/17.
 */
public class Message0x0200 extends Message {

    private static int[] offset = {
            4, 4, 4, 4, 2, 2, 2, 6
    };

    private static SimpleDateFormat dateFormat = new SimpleDateFormat();

    //基本信息部分
    private String warningType;
    private String status;
    private String latitude;
    private String longitude;
    private String altitude;
    private String gpsSpeed;
    private String direction;
    private String strDatetime;
    private String lightStatus;
    private String falutCode;
    private String actualPumpStatus;


    //附加信息部分
    private Map<String, String> addtion = new HashMap<>();


    public Message0x0200(String original) {
        setOriginal(original);
    }

    @Override
    protected void parseBody() {

        MessageHelper mh = getMessageHelper();
        List<String> valList = new ArrayList<>();

        for(int i = 0; i < offset.length; i++) {
            int loop = offset[i];
            StringBuilder sb = new StringBuilder();
            while (loop-- > 0)
                sb.append(mh.pop());
            valList.add(sb.toString());
        }

        warningType =   valList.get(0);
        status      =   valList.get(1);
        latitude    =   valList.get(2);
        longitude =   valList.get(3);
        altitude    =   valList.get(4);
        gpsSpeed    =   valList.get(5);
        direction   =   valList.get(6);
        strDatetime =   valList.get(7);

        //处理附加信息
        while (mh.getPos() < getOriginal().length() - 5) {
            String id = mh.pop();
            int len = hexString2Int(mh.pop());
            String val = "";
            while (len-- > 0)
                val += mh.pop();
            addtion.put(id, val);
        }
    }


    public float getGPSSpeed() {
        if (gpsSpeed != null)
            return (float) (hexString2Int(gpsSpeed) * 0.1);
        return -1;
    }

    /**
     *
     * @return 通过车速传感器计算出的速度
     */
    public float getSpeed() {
        String val = addtion.get("03");
        if (val != null)
            return (float) (hexString2Int(val) * 0.1);
        return -1;
    }

    public float getMileage() {
        String val = addtion.get("01");
        if (val != null)
            return (float) (hexString2Int(val) * 0.1);
        return -1;
    }

    /**
     *
     * @return 对应车上油量表读数，-1表示原始消息中没有此项
     */
    public int getFuelugauge(){
        String val = addtion.get("02");
        if (val != null)
            return (int) (hexString2Int(val) * 0.1);
        return -1;
    }

    public float getFuelGaugeInPercent() {
        String source = addtion.get("AA");
        if (source != null) {
            String fc = source.substring(12*2, 13*2);
            return (float) (hexString2Long(fc) * 0.004);
        }
        return -1;
    }

    public double getFuelConsumption() {
        String source = addtion.get("AA");
        if (source != null) {
            //兆达在200附加中的CAN数据为小端
            StringBuilder b = new StringBuilder();
            for (int i = 0; i <= 6; i += 2 ){
                //总油耗从第8字节开始 offset (8*2, 12*2)
                b.append(source.substring(22 - i, 24 - i));
            }
            return  hexString2Long(b.toString()) * 0.5;
        }
        return -1;
    }

    public float getWeight() {
        String source = addtion.get("E1");
        if (source != null) {
            //Big-endian
            String val = source.substring(2, 4) + source.substring(0, 2);
            return hexString2Long(val) / 10;
        }
        return 0;
    }



    public String getStrDatetime() {
        return strDatetime;
    }

    public String getFormatStrDatetime() {
        Date date = null;
        String result = null;
        try {
            dateFormat.applyPattern("yyMMddHHmmss");
            date = dateFormat.parse(strDatetime);
            dateFormat.applyPattern("yyyy-MM-dd HH:mm:ss");
            result = dateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


    public int getLatitude() {
        int val = 0;
        try {
            val = Integer.parseInt(latitude, 16);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return val;
    }

    public int getLongitude() {
        int val = 0;
        try {
            val = Integer.parseInt(longitude, 16);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return val;
    }

    public String getLightStatus() {
        return addtion.get("E2");
    }

    public String getFalutCode() {
        return addtion.get("E3");
    }

    public String getActualPumpStatus() {
        String content = addtion.get("EC");
        if (content != null) {
            int len = content.length();
            return content.substring(len - 2, len);
        }
        return null;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("latitude=").append(getLatitude()).append("\n");
        sb.append("longitude=").append(getLongitude()).append("\n");
        sb.append("GPS Speed=").append(getGPSSpeed()).append("\n");
        sb.append("Vehicle Speed=").append(getSpeed()).append("\n");
        sb.append("Mileage=").append(getMileage()).append("\n");
        sb.append("Fuel Gauge=").append(getFuelugauge()).append("\n");
        sb.append("Fuel Gauge Percent=").append(getFuelGaugeInPercent()).append("\n");
        sb.append("Fuel Consumption=").append(getFuelConsumption()).append("\n");
        sb.append("Weight=").append(getWeight()).append("\n");
        sb.append("灯状态=").append(getLightStatus()).append("\n");
        sb.append("故障码=").append(getFalutCode()).append("\n");
        sb.append("泵状态=").append(getActualPumpStatus()).append("\n");
        sb.append("time=").append(getFormatStrDatetime()).append("\n");

        return sb.toString();
    }


}
