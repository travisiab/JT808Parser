package cn.sih.parse;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.binary.StringUtils;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;


public class Message0x2035 extends Message {

    private static int[] offset = {
            17, 12, 10, 32
    };

    private String vin;
    private String vinHexString;
    private String ecuid;
    private String ecuidHexString;
    private String van;
    private String sn;
    private String snHexString;
    private String license;


    public Message0x2035(String original) {
        setOriginal(original);
    }

    @Override
    protected void parseBody() {

        MessageHelper mh = getMessageHelper();
        List<String> valList = new ArrayList<>();

        for(int i = 0; i < offset.length; i++) {
            int loop = offset[i];
            StringBuilder sb = new StringBuilder();
            while (loop-- > 0)
                sb.append(mh.pop());
            valList.add(sb.toString());
        }


        vin = decodeAscii(valList.get(0));
        vinHexString = valList.get(0);
        ecuid = valList.get(1);
        van = decodeAscii(valList.get(2));
        sn = decodeAscii(valList.get(3));
        snHexString = valList.get(3);

    }

    /**
     *  转换成16进制字符串消息
     * @return
     */
    public String toHexString() {
        return null;
    }

    public byte[] toBinary() {
        ByteBuffer buffer = ByteBuffer.allocate(100);
        char[] vin = this.vin.toCharArray();
        for (char c : vin) {
            buffer.put((byte) c);
        }

        char[] ecuid = this.ecuid.toCharArray();
        return null;

    }

    public byte[] getBinaryVin() {
        int size = 17;
        byte[] ascii = this.vin.getBytes(Charsets.US_ASCII);
        return padding(ascii, size);
    }

    public byte[] getBinaryEcuid() {
        int size = 12;
        long ecuid =  Long.valueOf(this.ecuid);
        byte[] bcd = BCD.DecToBCDArray(ecuid);
        return padding(bcd, size);

    }

    public byte[] getBinaryVan() {
        int size = 10;
        byte[] van = this.van.getBytes(Charsets.US_ASCII);
        return padding(van, size);
    }

    public byte[] getBinarySn() {
        int size = 32;
        byte[] sn = this.sn.getBytes(Charsets.US_ASCII);
        return padding(sn, size);
    }

    public byte[] getBinaryLicense() {
        return this.license.getBytes(Charset.forName("GB2312"));
    }


    private String getHexVin() {
        return String.valueOf(Hex.encodeHex(getBinaryVin(), false));
    }

    private byte[] binaryEcuid() {
        int size = 12;
        return null;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEcuid() {
        return ecuid;
    }

    public void setEcuid(String ecuid) {
        this.ecuid = ecuid;
    }

    public String getVan() {
        return van;
    }

    public void setVan(String van) {
        this.van = van;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("vin=").append(vin).append("\n");
        sb.append("vinHex=").append(vinHexString).append("\n");
        sb.append("ecuid=").append(ecuid).append("\n");
        sb.append("van=").append(van).append("\n");
        sb.append("sn=").append(sn).append("\n");
        sb.append("snHex=").append(snHexString).append("\n");
        return sb.toString();
    }

    private String decodeAscii(String hexString) {
        char[] ca = hexString.toCharArray();
        ByteBuffer byteBuffer = ByteBuffer.allocate(hexString.length() / 2);
        char zero = '0';
        for (int i = 0; i < ca.length; i += 2) {
            if (ca[i] == zero && ca[i+1] == zero) {
                continue;
            }
            byteBuffer.put(Byte.decode("0x" + ca[i] + ca[i+1]));
        }
        return new String(byteBuffer.array());
    }
}
