package cn.sih.parse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by travis on 15/10/20.
 */
public class Message0xA031 extends Message {

    private static int[] offset = {
            1, 3, 4, 3, 3
    };

    private String lockType;
    private String lockDate;
    private String checkCode;
    private String key;
    private String tcoid;



    public Message0xA031(String original) {
        setOriginal(original);
    }


    @Override
    protected void parseBody() {
        MessageHelper mh = getMessageHelper();

        List<String> valList = new ArrayList<>();

        for(int i = 0; i < offset.length; i++) {
            int loop = offset[i];
            StringBuilder sb = new StringBuilder();
            while (loop-- > 0)
                sb.append(mh.pop());
            valList.add(sb.toString());
        }


        lockType = valList.get(0);
        lockDate = valList.get(1);
        checkCode = valList.get(2);
        key = valList.get(3);
        tcoid = valList.get(4);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("lockType=").append(lockType).append("\n");
        sb.append("lockDate=").append(lockDate).append("\n");
        sb.append("checkCode=").append(checkCode).append("\n");
        sb.append("key=").append(key).append("\n");
        sb.append("tcoid=").append(tcoid).append("\n");
        return sb.toString();
    }
}
