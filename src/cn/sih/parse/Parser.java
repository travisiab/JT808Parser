package cn.sih.parse;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by travis on 3/29/16.
 */
public class Parser {

    private static final String sourceFile = "/Users/travis/Downloads/sources.txt";
    private static final String resultFile = "/Users/travis/Downloads/result.txt";

    List<String> sources = new ArrayList<>();
    List<Message0x0200> messages = new ArrayList<>();

    private SimpleDateFormat dateFormat = new SimpleDateFormat();

    public void doParse() {
        loadSourceFile();
        startParse();
        writeResultToFile();
        System.out.println("processing is completed...");
    }

    private void loadSourceFile() {
        String line = null;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(new File(sourceFile)));
            while ((line = reader.readLine()) != null) {
                sources.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Load source file completed...");
    }

    private void startParse() {
        for (String orginal : sources) {
            Message0x0200 msg = new Message0x0200(orginal);
            msg.parse();
            messages.add(msg);
        }
        System.out.println("Parse message completed...");
    }

    private void writeResultToFile() {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(new File(resultFile)));
            writeHeader(writer);
            for (Message0x0200 msg : messages) {
                writer.write(msg.getGPSSpeed()  + "\t");
                writer.write(msg.getSpeed()    + "\t");
                writer.write(msg.getMileage()   + "\t");
                writer.write(msg.getFormatStrDatetime()   + "\t\n");
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void writeHeader(BufferedWriter writer) throws IOException {
        writer.write("GPS Speed\t");
        writer.write("CAN Speed\t");
        writer.write("Total Mileage\t");
        writer.write("Date\t\n");
    }
}
