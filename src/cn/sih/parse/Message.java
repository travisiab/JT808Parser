package cn.sih.parse;

import org.apache.commons.codec.Charsets;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by travis on 15/10/17.
 */
public abstract class Message {
    private String original;

    private String id;
    private String attributes;
    private String mobile = "";
    private String serialNumber;

    private MessageHelper messageHelper = null;

    private ByteBuffer buffer = ByteBuffer.allocate(512);


    public void parse() {
        prepare();
        parseHeader();
        parseBody();
    }

    public void buildHeader() {

        //message id
        Integer id = Integer.decode("0x" + this.id);

        long m = Long.valueOf(this.mobile);
        byte[] mobile = padding(BCD.DecToBCDArray(m), 12);

    }

    protected void prepare() {
        messageHelper = new MessageHelper(original.replace("7d02", "7e").replace("7d01", "7d"));
    }
    
   
    protected void parseHeader() {
        //标识位
        String startFlag = messageHelper.pop();

        id = messageHelper.pop() + messageHelper.pop();
        attributes = messageHelper.pop() + messageHelper.pop();

        int loop = 6;
        while (loop-- > 0) {
            mobile += messageHelper.pop();
        }

        serialNumber = messageHelper.pop() + messageHelper.pop();
    }

    protected abstract void parseBody();




    public int getMessageHeaderLength() {
        int val = hexString2Int(attributes);
        int isSplitPackage = 0x2000 & val;
        if (isSplitPackage == 0)
            return 12;
        return 14;
    }

    public int getMessageBodyLength() {
        int val = hexString2Int(attributes);
        return 0x3ff & val;

    }

    public byte[] padding(byte[] src, int size) {
        if (src.length < size) {
            byte[] b = new byte[size];
            //left padding
            int d = size - src.length;
            for (int i = 0; i < size; i++) {
                if (i < d) {
                    b[i] = 0x00;
                } else {
                    b[i] = src[i - d];
                }
            }
            return b;
        }
        return Arrays.copyOf(src, size);
    }


    protected int hexString2Int(String str) {
        return Integer.parseInt(str, 16);
    }

    protected long hexString2Long(String str) {
        return Long.parseLong(str,16);
    }




    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAttributes() {
        return attributes;
    }

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public MessageHelper getMessageHelper() {
        return messageHelper;
    }

    public void setMessageHelper(MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("id=").append(id).append("\n");
        sb.append("mobile=").append(mobile).append("\n");
        sb.append("serialNumber=").append(serialNumber).append("\n");
        return  sb.toString();
    }
}
