package cn.sih.parse;

/**
 * Created by travis on 15/10/20.
 */
public class Message0x2036 extends Message {

    private String lockType;
    private String enabled;
    private String keyMatch;
    private String statusBit;
    private String falutCode;


    public Message0x2036(String original) {
        setOriginal(original);
    }

    @Override
    protected void parseBody() {
        getMessageHelper().shift(28);

        lockType = getMessageHelper().pop();
        enabled = getMessageHelper().pop();
        keyMatch = getMessageHelper().pop();
        statusBit = getMessageHelper().pop();
        falutCode = getMessageHelper().pop();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("lockType=").append(lockType).append("\n");
        sb.append("enabled=").append(enabled).append("\n");
        sb.append("keyMatch=").append(keyMatch).append("\n");
        sb.append("状态位=").append(statusBit).append("\n");
        sb.append("故障码=").append(falutCode).append("\n");
        return sb.toString();
    }
}
