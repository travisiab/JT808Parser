package cn.sih.simulator;

import org.apache.commons.codec.binary.Hex;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * @author Travis Bai
 */
public class Simulator {

    private static byte BOUNDARY = 126;
    private static byte BYTE_7D = 125;
    private static byte BYTE_02 = 2;
    private static byte BYTE_01 = 1;

    private String serverIp = "59.46.97.115";
    private int serverPort = 20401;


    public static void main(String[] args) {
        Simulator simulator = new Simulator();
//        byte[] data = simulator.data2035("7E25307E087D557E");
//        byte[] data = simulator.data2035("7E2035005F06468576056500034C5A464832355433584744333038353532036988370082260216094405000000000000000000000000000000000000000000003130303030303332363035373630353635000000000000000000000000000000C2B350453635393020202020C77E");
//        simulator.printByteInHexString(data);
        simulator.connect();
    }


    public void connect() {
        Socket socket = null;
        byte[] buff = new byte[100];

        BufferedInputStream input = null;
        DataOutputStream output = null;

        try {
            socket = new Socket(serverIp, serverPort);
            input = new BufferedInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());

            while (true) {
                output.write(buildData2035());
                output.flush();
                System.out.println("2035 sent..... ");

                int size = input.read(buff, 0, buff.length);
//                System.out.printf("receive: %02X", buff);
                if (size != -1) {
                    byte[] readData = Arrays.copyOfRange(buff, 0, size);
                    System.out.println("Receive:" + String.valueOf(Hex.encodeHex(readData, false)));
                } else {
                    System.out.print("no more response from server......");
                }

                Thread.sleep(10000);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private byte[] buildData2035() {
        //vin   =   LZFH25T3XGD308552
        //通信号 =   064685760565
//        String hexData = "7E2035005F06468576056500034C5A464832355433584744333038353532036988370082260216094405000000000000000000000000000000000000000000003130303030303332363035373630353635000000000000000000000000000000C2B350453635393020202020C77E";

        //vin   =   LZFF25T40GD309242
        //通信号 =   064685760565
//        String hexData = "7E2035005F06468576056500034C5A464632355434304744333039323432036988370032040316202842000000000000000000000000000000000000000000003130303030303332363031313639313033000000000000000000000000000000D4C647363933373820202020B17E";
//        String hexData = "7E2035005F06467511723500034C5A464632355434304744333039323432036988370032040316202842000000000000000000000000000000000000000000003130303030303332363031313639313033000000000000000000000000000000D4C647363933373820202020B17E";
        String hexData = "7E2035005F06467511714200034C5A464632355434304744333039323432036988370032040316202842000000000000000000000000000000000000000000003130303030303332363031313639313033000000000000000000000000000000D4C647363933373820202020B17E";
        return data2035(hexData);
    }

    /**
     *
     * @param hexStringData 已转义还原的16进制字符串
     * @return 转义后二进制报文
     */
    public byte[] data2035(String hexStringData) {
        char[] charArray = hexStringData.toCharArray();
        int tempSize = hexStringData.length() * 2;
        ByteBuffer buffer = ByteBuffer.allocate(tempSize);
        int size = 2;

        buffer.put(BOUNDARY);
        byte crc = 0x00;
        crc ^= BOUNDARY;
        for (int i = 2; i <= charArray.length - 6; i+=2) {
            int intVal = Integer.decode("0x" + charArray[i] + charArray[i+1]);
            byte v = (byte) intVal;
            if (BOUNDARY == v) {
                buffer.put(BYTE_7D).put(BYTE_02);
                size += 2;
            } else if (BYTE_7D == v) {
                buffer.put(v).put(BYTE_01);
                size += 2;
            } else {
                buffer.put(v);
                size++;
            }
            crc ^= v;
        }
        crc ^= BOUNDARY;
        buffer.put(crc);
        buffer.put(BOUNDARY);

        int binarySize = size + 1;
        ByteBuffer dataBuffer = ByteBuffer.allocate(binarySize);
        dataBuffer.put(buffer.array(), 0, binarySize);

        byte[] binaryData = dataBuffer.array();
        System.out.println(String.valueOf(Hex.encodeHex(binaryData, false)));
        return binaryData;
    }

    public void printByteInHexString(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for (byte b : data) {
            sb.append(String.format("%02X", b));
        }
        System.out.println(sb.toString());
    }

}
