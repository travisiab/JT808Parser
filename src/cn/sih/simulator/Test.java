package cn.sih.simulator;

import cn.sih.parse.Message;
import cn.sih.parse.Message0x2035;
import org.apache.commons.codec.binary.Hex;

import java.util.Arrays;

/**
 * @author Travis Bai
 */
public class Test {
    public static void main(String[] args) {
        int i = Integer.decode("0xFF");

        byte b = (byte) i;
        System.out.println(String.format("%2X", i));
        System.out.println(String.format("%2X", b));
        System.out.println(b);

        Message0x2035 message0x2035 = new Message0x2035("test");
        message0x2035.setVin("HD2341");
        message0x2035.setEcuid("123");
        System.out.println(Hex.encodeHexString(message0x2035.getBinaryVin()));
        System.out.println(Hex.encodeHexString(message0x2035.getBinaryEcuid()));




    }
}
